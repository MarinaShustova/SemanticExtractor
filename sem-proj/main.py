from library import Library
from dialog import run_windows
import pickle


def fill_library(library):
    with open('resources/lib.pkl', 'rb') as f1:
        library = pickle.load(f1)
    f1.close()
    return library


def main():
    library = Library()
    library = fill_library(library)
    run_windows(library)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        exit()
