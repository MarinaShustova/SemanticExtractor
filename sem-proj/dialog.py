import sys
from PyQt5 import QtWidgets
import design.design2 as mainWindow
import design.templates_design as templateWindow
import design.models_design as modelsWindow
from analyzer import stat_similarity

global library


class MyWindow(QtWidgets.QMainWindow, mainWindow.Ui_MainWindow):

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.initUI()

    def initUI(self):
        self.open_file.clicked.connect(self.show_dialog)
        self.templates_list.clicked.connect(self.show_library)
        self.models.clicked.connect(self.show_models)
        self.setWindowTitle('Semantic Extractor')

    def show_dialog(self):
        fname = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file', '/home')[0]

        if (fname != ''):
            f = open(fname, 'r')

            with f:
                data = f.read()
                self.textEdit.setText(data)

    def show_library(self):
        self.templates = TemplatesWindow()
        self.templates.show()

    def show_models(self):
        self.models = ModelsWindow()
        self.models.show()


class TemplatesWindow(QtWidgets.QDialog, templateWindow.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.fill_list()
        self.setWindowTitle('Шаблоны')
        self.set_compare_button()
        self.set_show_button()

    def set_compare_button(self):
        self.pushButton.clicked.connect(self.show_comparison)

    def set_show_button(self):
        self.pushButton_2.clicked.connect(self.show_template_info)

    def show_template_info(self):
        items = self.listWidget.selectedItems()
        length = len(items)
        for i in range(length):
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            msg.setWindowTitle("Информация о шаблоне")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
            t = library.get_template_by_name(items[i].text())
            msg.setText("Шаблон {}".format(t.get_name()))
            msg.setDetailedText("Импликации шаблона:\n{}".format(self.cute_print(t.implications)))
            msg.exec_()

    def show_comparison(self):
        items = self.listWidget.selectedItems()
        if len(items) != 2:
            self.show_warn_dialog()
        else:
            templ1 = library.get_template_by_name(items[0].text())
            templ2 = library.get_template_by_name(items[1].text())
            self.describe_intersection(templ1.compare(templ2))

    def describe_intersection(self, intersect):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Information)
        msg.setWindowTitle("Результат сравнения")
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        if len(intersect) == 0:
            msg.setText("Эти шаблоны независимы")
            msg.setInformativeText("У шаблонов нет совпадающих импликаций")
            msg.exec_()
        else:
            msg.setText("Один шаблон расширяет другой")
            msg.setInformativeText("У этих шаблонов {} совпадающих импликаций".format(len(intersect)))
            msg.setDetailedText("Совпадающие импликации:\n{}".format(self.cute_print(intersect)))
            msg.exec_()

    def cute_print(self, implications):
        a = ""
        for i in implications:
            a += i.left_expr + " -> " + i.right_expr + "\n"
        return a

    def show_warn_dialog(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Warning)

        msg.setText("Шаблоны можно сравнивать только попарно!")
        msg.setInformativeText("Пожалуйста, выберите два шаблона для сравнения")
        msg.setWindowTitle("Предупреждение")
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()

    def fill_list(self):
        for template in library.templates:
            self.listWidget.addItem(template.name)


class ModelsWindow(QtWidgets.QDialog, modelsWindow.Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.tableWidget.setRowCount(len(library.models))
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setHorizontalHeaderLabels(
            ('Модель', 'Шаблон')
        )
        self.setWindowTitle('Модели')
        self.fill_table()
        self.set_compare_button()
        self.set_show_button()

    def set_compare_button(self):
        self.compare.clicked.connect(self.show_comparison)

    def set_show_button(self):
        self.show_single.clicked.connect(self.show_model_info)

    def show_model_info(self):
        items = self.tableWidget.selectedItems()
        length = len(items)
        for i in range(length):
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Information)
            msg.setWindowTitle("Информация о модели")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
            m = library.get_model_by_name(items[i].text())
            msg.setText("Модель {}".format(m.get_name()))
            msg.setDetailedText("Импликации модели:\n{}".format(self.cute_model_print(m)))
            msg.exec_()

    def cute_model_print(self, model):
        a = ""
        for impl in model.template.implications:
            a += model.constants[impl.id][impl.left_expr] + " -> " + model.constants[impl.id][impl.right_expr] + "\n"
        return a

    def show_comparison(self):
        items = self.tableWidget.selectedItems()
        if len(items) != 2:
            self.show_warn_dialog()
        else:
            model1 = library.get_model_by_name(items[0].text())
            model2 = library.get_model_by_name(items[1].text())
            self.describe_intersection(model1.compare(model2))

    def describe_intersection(self, intersect):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Information)
        msg.setWindowTitle("Результат сравнения")
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        if intersect == False:
            msg.setText("Эти модели независимы")
            msg.setInformativeText("У шаблонов этих моделей нет ничего общего")
            msg.exec_()
        else:
            msg.setText("У этих моделей есть потенциальные противоречия")
            msg.setInformativeText("У этих моделей {} несовпадающих импликаций".format(len(intersect)))
            msg.setDetailedText("Потенциальные противоречия:\n{}".format(self.cute_print(intersect)))
            msg.exec_()

    def cute_print(self, contradictions):
        a = ""
        for i in contradictions:
            similarity = stat_similarity(i[1].split()[0], i[2].split()[0])
            if similarity != 'Unknown' and float(similarity) >= 0.5:
                res = " [OK]\n"
            else:
                res = " [WARN]\n"
            a += i[0] + ": " + i[1] + " и " + i[2] + res
        return a

    def show_warn_dialog(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Warning)

        msg.setText("Модели можно сравнивать только попарно!")
        msg.setInformativeText("Пожалуйста, выберите две модели для сравнения")
        msg.setWindowTitle("Предупреждение")
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()

    def fill_table(self):
        row = 0
        for model in library.models:
            cellinfo = QtWidgets.QTableWidgetItem(model.get_name())
            self.tableWidget.setItem(row, 0, cellinfo)
            cellinfo = QtWidgets.QTableWidgetItem(model.template.get_name())
            self.tableWidget.setItem(row, 1, cellinfo)
            row += 1


def run_windows(lib):
    global library
    library = lib
    app = QtWidgets.QApplication(sys.argv)
    ex = MyWindow()
    ex.show()
    app.exec_()


if __name__ == '__main__':
    run_windows()
