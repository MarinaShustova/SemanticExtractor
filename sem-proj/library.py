#!/usr/bin/env python3
class Library:

    def __init__(self):
        self.templates = []
        self.models = []

    def add_template(self, template):
        self.templates.append(template)

    def add_model(self, model):
        self.models.append(model)

    def get_template_by_name(self, name):
        for t in self.templates:
            if t.get_name() == name:
                return t

    def get_model_by_name(self, name):
        for m in self.models:
            if m.get_name() == name:
                return m
