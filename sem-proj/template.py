#!/usr/bin/env python3
from Implication import Implication


class Template:

    def __init__(self, name):
        self.name = name
        self.implications = []
        self.actors = []
        self.preconditions = []
        self.conclusions = []
        self.constants = {}

    def add_implication(self, implication):
        self.implications.append(implication)

    def add_actor(self, new_actor):
        self.actors.append(new_actor)

    def add_link(self, prec, cons):
        self.preconditions.append(prec)
        self.conclusions.append(cons)
        impl = Implication(prec, cons)
        self.implications.append(impl)
        return impl.id

    def get_name(self):
        return self.name

    def custom_contains(self, item, container):
        for an_item in container:
            if item.left_expr == an_item.left_expr and item.right_expr == an_item.right_expr:
                return True
        return False

    def compare(self, another):
        intersection = []
        for implication in self.implications:
            if self.custom_contains(implication, another.implications):
                intersection.append(implication)
        return intersection
