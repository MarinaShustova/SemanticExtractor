import uuid


class Implication:
    def __init__(self, left, right):
        self.id = uuid.uuid4()
        self.left_expr = left
        self.right_expr = right
