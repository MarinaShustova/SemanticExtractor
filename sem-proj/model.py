from template import Template


class Model:
    def __init__(self, template, name):
        self.template = template
        self.constants = {}
        self.name = name

    def add_signification(self, impl_id, p_value, c_value):
        for index, value in enumerate(self.template.implications):
            if value.id == impl_id:
                self.constants[impl_id] = {self.template.implications[index].left_expr: p_value,
                                           self.template.implications[index].right_expr: c_value}

    def compare(self, another):
        potential_contradictions = []
        if self.template == another.template:
            for impl in self.template.implications:
                if self.constants[impl.id][impl.right_expr] != another.constants[impl.id][impl.right_expr]:
                    potential_contradictions.append([self.constants[impl.id][impl.left_expr], self.constants[impl.id][impl.right_expr], another.constants[impl.id][impl.right_expr]])
            return potential_contradictions
        return False

    def get_name(self):
        return self.name
