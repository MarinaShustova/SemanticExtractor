import pickle
from library import Library
from template import Template
from model import Model


class Infomaker:
    def __init__(self):
        self.lib = Library()

    def fill_library(self):
        templ1 = Template("Курс")
        id = templ1.add_link("Модификация курса рубля", "Изменение реакции экономики на внешние шоки")
        id2 = templ1.add_link("Модификация курса рубля", "Влияние на валюту")
        model = Model(templ1, "Плавающий курс")
        model.add_signification(id, "Установление плавающего курса", "Ускорение адаптации к внешним шокам")
        model.add_signification(id2, "Установление плавающего курса", "Стабильность национальной валюты")
        self.lib.add_model(model)
        self.lib.add_template(templ1)

        templ2 = Template("Спрос")
        id = templ2.add_link("Изменение спроса на продукт", "Влияние на компанию")
        id2 = templ2.add_link("Влияние на компанию", "Влияние на работников")
        model2 = Model(templ2, "Молочная компания")
        model2.add_signification(id, "Стали предпочитать соевое молоко обычному", "Разорение компании \"Dean Foods\"")
        model2.add_signification(id2, "Разорение компании \"Dean Foods\"", "Увольнение многих людей")
        self.lib.add_template(templ2)
        self.lib.add_model(model2)

        model6 = Model(templ2, "Молочная компания 2")
        model6.add_signification(id, "Стали предпочитать совое молоко обычному",
                                 "Банкротство компании \"Dean Foods\"")
        model6.add_signification(id2, "Банкротство компании \"Dean Foods\"", "Повышение многих людей")
        self.lib.add_model(model6)

        templ3 = Template("Курс расширение")
        id = templ3.add_link("Модификация курса рубля", "Изменение реакции экономики на внешние шоки")
        id2 = templ3.add_link("Модификация курса рубля", "Влияние на валюту")
        id3 = templ3.add_link("Модификация курса рубля", "Влияние на государство")
        model3 = Model(templ3, "Стабильный курс")
        model3.add_signification(id, "Установление строгого валютного коридора",
                                 "Экономика быстрее адаптируеся к внешним шокам")
        model3.add_signification(id2, "Установление строгого валютного коридора",
                                 "Оживление спроса на рубль")
        model3.add_signification(id3, "Установление строгого валютного коридора",
                                 "Реализация национальных проектов")
        self.lib.add_model(model3)
        self.lib.add_template(templ3)

        templ4 = Template("Здравоохранение")
        id = templ4.add_link("Модификация инфраструктуры", "Влияние на здоровье людей")
        id2 = templ4.add_link("Модификация систем здравоохранения", "Влияние на здоровье людей")
        model4 = Model(templ4, "Смертность")
        model4.add_signification(id, "Созданы 94 перинатальных центра",
                                 "Снизилась материнская и младенческая смертность")
        model4.add_signification(id2, "Развитие системы комплексной профилактики",
                                 "Вдвое снизилась заболеваемость туберкулёзом и втрое - смертность от этого заболевания")
        self.lib.add_model(model4)
        self.lib.add_template(templ4)

        templ5 = Template("Наука")
        id = templ5.add_link("Новая гипотеза", "Влияние на существующую систему знаний")
        id2 = templ5.add_link("Условия среды", "Чему способствуют")
        id3 = templ5.add_link("Условия проведения эксперимента", "Влияние на результат")
        model5 = Model(templ5, "Зарождение жизни")
        model5.add_signification(id, "Жизнь могла зародиться в глубоководных термальных источниках",
                                 "Ставит под сомнение идею о том, что первые клетки возникли в тёплых мелких водоёмах")
        model5.add_signification(id2, "Высокие солёность и щёлочность",
                                 "Спонтанная сборка молекул в пузырьки")
        model5.add_signification(id3, "Сложная органика поглащается уже существующими живыми существами",
                                 "Невозможно провести экперимент для подтверждения возникновения жизни в пруду")
        self.lib.add_model(model5)
        self.lib.add_template(templ5)

        with open('resources/lib.pkl', 'wb') as f1:
            pickle.dump(self.lib, f1)
        f1.close()


if __name__ == '__main__':
    info = Infomaker()
    info.fill_library()
