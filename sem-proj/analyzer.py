import requests


def api_similarity(w1, w2, m='tayga_upos_skipgram_300_2_2019'):
    url = '/'.join(['https://rusvectores.org', m, w1 + '__' + w2, 'api', 'similarity/'])
    r = requests.get(url, stream=True)
    return r.text.split('\t')[0]

def stat_similarity(statement1, statement2):
    s1 = statement1 + '_NOUN'
    s2 = statement2 + '_NOUN'
    return api_similarity(s1, s2)


if __name__ == '__main__':
    MODEL = 'tayga_upos_skipgram_300_2_2019'
    ret = api_similarity('разорение_NOUN', 'банкротство_NOUN', MODEL)
    print(ret)
