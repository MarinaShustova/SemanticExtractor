import unittest
import sys
sys.path.append("../")
from library import Library
from template import Template
from model import Model
from Implication import Implication

class TestLibraryMethods(unittest.TestCase):
    def test_add_template_add_model(self):
        lib = Library()
        templ = Template("test template 1")
        model = Model(templ, "test model 1")
        lib.add_template(templ)
        self.assertEqual([templ], lib.templates, "Templates lists should be equal")
        lib.add_model(model)
        self.assertEqual([model], lib.models, "Models lists should be equal")

    def test_intersect_templates(self):
        templ1 = Template("test template 1")
        templ2 = Template("test template 2")
        id1 = templ1.add_link("precondition 1", "conclusion 1")
        id11 = templ1.add_link("precondition 2", "conclusion 2")
        id2 = templ2.add_link("precondition 1", "conclusion 1")
        intersection = templ1.compare(templ2)
        self.assertEqual(intersection, [templ1.implications[0]])

    # def test_compare_models_true(self):
    #     templ1 = Template("test template 1")
    #     templ1.add_link("precondition 1", "conclusion 1")
    #     model1 = Model(templ1, "Model 1")
    #     model2 = Model(templ1, "Model 2")
    #     self.assertEqual(model2.compare(model1), True)

    def test_compare_models_false(self):
        templ1 = Template("test template 1")
        templ2 = Template("test template 2")
        templ1.add_link("precondition 1", "conclusion 1")
        templ2.add_link("precondition 1", "conclusion 1")
        model1 = Model(templ1, "Model 1")
        model2 = Model(templ2, "Model 2")
        self.assertEqual(model2.compare(model1), False)



if __name__ == '__main__':
    unittest.main()
