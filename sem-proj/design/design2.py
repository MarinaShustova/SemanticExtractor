# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'text_edit_main.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(557, 502)
        MainWindow.setMinimumSize(QtCore.QSize(557, 502))
        MainWindow.setMaximumSize(QtCore.QSize(557, 502))
        MainWindow.setAutoFillBackground(False)
        MainWindow.setStyleSheet("QMainWindow{background-image: url(:/background/BACKGROUND_PATTERN.png);}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setStyleSheet("")
        self.centralwidget.setObjectName("centralwidget")
        self.templates_list = QtWidgets.QPushButton(self.centralwidget)
        self.templates_list.setGeometry(QtCore.QRect(30, 90, 113, 32))
        self.templates_list.setObjectName("templates_list")
        self.open_file = QtWidgets.QPushButton(self.centralwidget)
        self.open_file.setGeometry(QtCore.QRect(30, 40, 113, 32))
        self.open_file.setMinimumSize(QtCore.QSize(113, 32))
        self.open_file.setMaximumSize(QtCore.QSize(113, 32))
        self.open_file.setToolTip("")
        self.open_file.setToolTipDuration(5)
        self.open_file.setStyleSheet("")
        self.open_file.setIconSize(QtCore.QSize(32, 32))
        self.open_file.setObjectName("open_file")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setGeometry(QtCore.QRect(170, 20, 361, 441))
        self.textEdit.setMinimumSize(QtCore.QSize(261, 311))
        self.textEdit.setObjectName("textEdit")
        self.save_button = QtWidgets.QPushButton(self.centralwidget)
        self.save_button.setGeometry(QtCore.QRect(300, 460, 113, 32))
        self.save_button.setObjectName("save_button")
        self.models = QtWidgets.QPushButton(self.centralwidget)
        self.models.setGeometry(QtCore.QRect(30, 140, 113, 32))
        self.models.setObjectName("models")
        self.textEdit.raise_()
        self.open_file.raise_()
        self.templates_list.raise_()
        self.save_button.raise_()
        self.models.raise_()
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.templates_list.setText(_translate("MainWindow", "Шаблоны"))
        self.open_file.setText(_translate("MainWindow", "Открыть файл"))
        self.textEdit.setPlaceholderText(_translate("MainWindow", "Please open file to start working"))
        self.save_button.setText(_translate("MainWindow", "Сохранить"))
        self.models.setText(_translate("MainWindow", "Модели"))

from design import resources_rc
