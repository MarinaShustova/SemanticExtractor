# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'models.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(353, 387)
        Dialog.setStyleSheet("QDialog{background-image: url(:/background/BACKGROUND_PATTERN.png);}")
        self.tableWidget = QtWidgets.QTableWidget(Dialog)
        self.tableWidget.setGeometry(QtCore.QRect(20, 10, 311, 331))
        self.tableWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(0)
        self.tableWidget.setRowCount(0)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(True)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(150)
        self.tableWidget.verticalHeader().setCascadingSectionResizes(True)
        self.compare = QtWidgets.QPushButton(Dialog)
        self.compare.setEnabled(True)
        self.compare.setGeometry(QtCore.QRect(50, 350, 113, 32))
        self.compare.setObjectName("compare")
        self.show_single = QtWidgets.QPushButton(Dialog)
        self.show_single.setEnabled(True)
        self.show_single.setGeometry(QtCore.QRect(180, 350, 113, 32))
        self.show_single.setObjectName("show_single")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.compare.setText(_translate("Dialog", "Сравнить"))
        self.show_single.setText(_translate("Dialog", "Показать"))

from design import templates_rc
